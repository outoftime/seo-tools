# ~*~ encoding: utf-8 ~*~
from flask.ext.login import UserMixin

from app import db


class User(UserMixin, db.Model):

    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(64), index=True, unique=True)
    password = db.Column(db.String(64))

    def __repr__(self):
        return '<User #{}>'.format(self.username)
