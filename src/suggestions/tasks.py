# ~*~ encoding: utf-8 ~*~
from db import RedisDatabase


class Task(object):

    @staticmethod
    def get_task():
        return RedisDatabase.instance().get_task()

    @staticmethod
    def get_task_suggestion():
        return RedisDatabase.instance().get_task_suggestion()

    @staticmethod
    def create_task(keyword=None, depth=None, with_numbers=False):
        task = dict(keyword=keyword, depth=depth, with_numbers=with_numbers)
        RedisDatabase.instance().add_task(task)

    @staticmethod
    def create_task_suggestion(keyword=None, sibling=None,
                               depth=None, with_numbers=False):
        rdb = RedisDatabase.instance()
        task = dict(
            keyword=keyword,
            sibling=sibling,
            depth=depth,
            with_numbers=with_numbers)
        rdb.add_task_suggestion(task)

